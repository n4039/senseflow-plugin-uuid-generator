# SenseFlow UUID Generator

This SenseFlow plugin offers one new node which generates a uuidv4 key and assigns it to some msg property of your choice.

## Installation and Running

Clone the [SenseFlow](https://gitlab.com/multicast/senseup/SenseFlow) runtime, move to its directory and perform a local installation through npm.

### In SenseFlow directory
Install dependencies:
```
npm i
```

Install this project: 
```
npm install @senseup/senseflow-plugin-uuid-generator
```

Then run SenseFlow:

```
npm start
```