import { Node, NodeDef, NodeInitializer, NodeMessage } from "node-red";
import { v4 as uuidv4 } from 'uuid';

interface EnvVarGetNodeDef extends NodeDef {
    output?: string | undefined
}

let NODE: Node;
let CONFIG: EnvVarGetNodeDef;

function nodeOnInput(msg: NodeMessage) {
    
    if (typeof CONFIG.output !== 'undefined' && CONFIG.output.toString()) 
        msg[CONFIG.output.toString()] = uuidv4();
    
    NODE.send(msg);
}

const generate_UUID: NodeInitializer = function (RED){
    function generateUUID (config: NodeDef){
        RED.nodes.createNode(this, config);
        NODE = this;
        CONFIG = config;
        NODE.on('input', nodeOnInput);
    }
    RED.nodes.registerType("generate-uuid", generateUUID);
}

export = generate_UUID;